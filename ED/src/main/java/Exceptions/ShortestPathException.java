package Exceptions;

/**
 * Exceção para o método de djiskra
 */
public class ShortestPathException extends Exception {
    public ShortestPathException(String msg) {
        super(msg);
    }
}
