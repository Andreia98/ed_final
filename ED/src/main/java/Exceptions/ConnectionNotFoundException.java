package Exceptions;

public class ConnectionNotFoundException extends Exception {
    /**
     * Contructs a new instance of ConnectionNotFoundException with a specified detail message
     * @param msg the detail message
     */
    public ConnectionNotFoundException(String msg) {
        super(msg);
    }
    /**
     * Creates a new instance of ConnectionNotFoundException without a specified detail message
     */
    public ConnectionNotFoundException() {

    }
}
