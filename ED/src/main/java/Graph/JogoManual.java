package Graph;

import ED_Ficha5.ArrayUnorderedList;
import Exceptions.ConnectionNotFoundException;
import Interfaces.JogoManualADT;


public class JogoManual<T> extends Network<T> implements JogoManualADT<T> {
    private T posicao_inicial;
    private T posicao_actual;
    private T posicao_final;
    private int vida;
    private int count = 0;

    /**
     * Método responsável por retornar um array UnorderList com as ligações para a posição actual
     * @return ArrayUnorderedList com as ligações para a posição actual
     */
    public ArrayUnorderedList<T> getLigacoesAposento() {
        ArrayUnorderedList<T> ligacoes_possiveis = new ArrayUnorderedList();
        for (int j = 0; j < numVertices; j++) {
            if (adjMatrix[this.getIndex(posicao_actual)][j] != null) {
                if (ligacoes_possiveis.isEmpty()) {
                    ligacoes_possiveis.addToFront(vertices[j]);
                } else {
                    ligacoes_possiveis.addToRear(vertices[j]);
                }
            }
        }
        return ligacoes_possiveis;
    }

    /**
     * Método responsável por alterar a posição atual do utilizador para a nova posição e
     * por alterar a vida.
     * @param next_position nova posição
     * @return True quando a posição actual for alterada
     * @throws ConnectionNotFoundException
     */
    public boolean changePosition(T next_position) throws ConnectionNotFoundException {
        if (adjMatrix[getIndex(posicao_actual)][getIndex(next_position)] >= 0) {

            vida -= adjMatrix[getIndex(posicao_actual)][getIndex(next_position)];
            this.posicao_actual = next_position;
            count++;

        } else {
            throw new ConnectionNotFoundException("Não encontrada ligação entre os vértices");
        }
        return true;
    }

    /**
     * Método responsável por retornar se o jogador está ou não vivo
     * @return true se a vida for maior que 0
     */
    public boolean isAlive() {
        return vida > 0;
    }

    /**
     * Método responsável por multiplicar o dano conforme o nivel selecionado
     *
     * @param level nivel escolhido pelo utilizador
     */
    public void dificuldade(int level) {
        for (int i = 0; i < numVertices; i++) {
            for (int j = 0; j < numVertices; j++) {
                if (adjMatrix[i][j] != null && adjMatrix[i][j] > 0) {
                    adjMatrix[i][j] = adjMatrix[i][j] * level;
                }
            }
        }
    }

    /**
     * Método responsável por definir a vida
     * @param vida vida do jogador
     */
    public void setVida(int vida) {
        this.vida = vida;
    }

    /**
     * Método responsável por retornar a vida do jogador
     * @return vida do jogador
     */
    public int getVida() {
        return vida;
    }

    /**
     * Método responsável por definir a posição inicial do jogador
     * @param posicaoInicial posição inicial do jogador
     */
    public void setPosicao_inicial(T posicaoInicial) {
        this.posicao_inicial = posicaoInicial;
    }

    /**
     * Método responsável por definir a posição actual do jogador
     * @param posicaoActual posição actual do jogador
     */
    public void setPosicao_actual(T posicaoActual) {
        this.posicao_actual = posicaoActual;
    }

    /**
     * Método responsável por definir a posição final do jogador
     * @param posicaoFinal posição final do jogador
     */
    public void setPosicao_final(T posicaoFinal) {
        this.posicao_final = posicaoFinal;
    }

    /**
     * Método responsável por retornar se o jogo está ou não completo
     * @return true se a posição actual do jogador for igual à posição final
     */
    public boolean isComplete() {
        return this.posicao_actual == posicao_final;
    }

    /**
     * Método responsável por retornar o numero de caminhos utilizados pelo jogador
     * @return numero de caminhos do jogador
     */
    public int getCount() {
        return count;
    }

    /**
     * Método responsável por retornar a posição inicial do jogador
     * @return posição inicial do jogador
     */
    public T getPosicao_inicial() {
        return posicao_inicial;
    }

    /**
     * Método responsável por retornar a posição actual do jogador
     * @return posição actual do jogador
     */
    public T getPosicao_actual() {
        return posicao_actual;
    }

    /**
     * Método responsável por retornar uma string de output com os caminhos possiveis
     * @return string formato HTML com caminhos existentes possiveis
     */
    public String printGraph(){
        String str = "";
        for(int i = 0; i<numVertices; i++){
            for (int j=0; j< numVertices;j++){
                if(adjMatrix[i][j] != null){
                    str += "<br/>[" + vertices[i] +"]" +"<font color='red'>--></font>" + "[" +vertices[j] +"]" +"";
                }
            }
        }
        return str;
    }

}
