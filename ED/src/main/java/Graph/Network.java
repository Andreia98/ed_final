/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Graph;

import Exceptions.ShortestPathException;
import Interfaces.NetworkADT;

import java.util.NoSuchElementException;

/**
 * Implementação de uma rede com base num grafo implementado com recurso
 * a uma matriz de adjacências
 * 
 * @author Pedro e Andreia
 * @param <T>
 */
public class Network<T> extends Graph<T> implements NetworkADT<T> {

    /**
     * Método que permite adicionar uma aresta. Uma aresta é uma ligação entre
     * dois vértices existentes 
     * 
     * @param vertice1 vertice de uma ponta da aresta
     * @param vertice2 vertice da outra ponta da aresta
     * @param peso peso da aresta
     */
    @Override
    public void addEdge(T vertice1, T vertice2, double peso) {
        
        // Obter os índices dos vértices
        int indexVertex1 = getIndex(vertice1);
        int indexVertex2 = getIndex(vertice2);
        
        /** 
         * < Se os dois indices forem válidos> 
         * Significa que os vértices existem
         */
        if (indexIsValid(indexVertex1) && indexIsValid(indexVertex2)) {
            
            /**
             * Adicionar a aresta com o peso definido
             * Significa que agora existe uma ligação entre os dois vértices
             */
            adjMatrix[indexVertex2][indexVertex1] = peso;
            
        } else
            throw new NoSuchElementException("Algum dos vértices não existe");
        
    }

    /**
     * Método que retorna o custo do caminho mais curto entre dois vertices
     * 
     * @param vertice1 primeiro vértice
     * @param vertice2 segundo vértice
     * @return o custo do caminho mais curto entre os dois vertices
     */
    @Override
    public double shortestPathWeight(T vertice1, T vertice2) throws ShortestPathException {
        Double custo = null;
        int origem = this.getIndex(vertice1);
        int distino = this.getIndex(vertice2);

        if (this.indexIsValid(origem) && this.indexIsValid(distino)) {
            custo = this.dijkstra(origem, distino);
        }

        return custo;
    }

    /**
     * Método que retorna o custo do caminho mais curto.
     * É calculado com a ajuda do Dijkstra Algoritmo
     * @param origem vertice origem
     * @param destino vertice destino
     * @return custo/peso do caminho mais curto
     */
    private double dijkstra(int origem, int destino) throws ShortestPathException {
        // lista para a distancia de cada vertice
        double[] peso = new double[this.numVertices];

        // lista que marca os vertices visitados
        boolean[] vertices_visitados = new boolean[this.numVertices];

        for (int i = 0; i < this.numVertices; i++) {
            peso[i] = Double.MAX_VALUE;
            vertices_visitados[i] = false;
        }

        //define o peso de origem a 0
        peso[origem] = 0;

        for (int i = 0; i < this.numVertices - 1; i++) {
            //recebe o index do vertice_menorCusto
            int vertice_menorCusto = this.minDistance(peso, vertices_visitados);

            //define no index do vertice com menos custo a true, ou seja, visitado
            vertices_visitados[vertice_menorCusto] = true;


            for (int j = 0; j < this.numVertices; j++) {
                if (!vertices_visitados[j] && this.adjMatrix[vertice_menorCusto][j] != null && peso[vertice_menorCusto] != Double.MAX_VALUE
                        && peso[vertice_menorCusto] + this.adjMatrix[vertice_menorCusto][j] < peso[j]) {
                    peso[j] = peso[vertice_menorCusto] + this.adjMatrix[vertice_menorCusto][j];
                }
            }
        }

        if(!vertices_visitados[destino]){
            throw new ShortestPathException("Não existe Caminho possivel");
        }

        //retornar o peso da origem ao destino
        return peso[destino];
    }



}
