/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Graph;


import DoubleLinkedList.DoubleLinkedUnorderedList;
import ED_Ficha5.ArrayUnorderedList;
import Exceptions.EmptyCollectionException;
import Interfaces.GraphADT;
import Queue.LinkedQueue;
import Stack.LinkedStack;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Implementação de um grafo com recurso a uma matriz de adjacências
 *
 * @param <T>
 * @author Andreia e Pedro
 */
public class Graph<T> implements GraphADT<T> {

    // Capacidade por defeito
    protected final int DEFAULT_CAPACITY = 5;

    // Número de vértices do grafo
    protected int numVertices;

    // Matriz de adjacencias
    protected Double[][] adjMatrix;

    // Valores dos vértices
    protected T[] vertices;

    // Criar um grafo vazio
    public Graph() {
        this.numVertices = 0;
        this.adjMatrix = new Double[DEFAULT_CAPACITY][DEFAULT_CAPACITY];
        this.vertices = (T[]) (new Object[DEFAULT_CAPACITY]);
    }

    /**
     * Método que permite adicionar um novo vértice no grafo. Inicialmente o
     * grafo não possui ligações (arestas)
     *
     * @param vertex novo vértice
     */
    @Override
    public void addVertex(T vertex) {

        //Se o grafo estiver cheio, aumenta o tamanho da matriz
        //de adjacencias do array de vertices
        if (numVertices == vertices.length) {
            expandCapacity();
        }

        //Adicionar o vertice no array
        vertices[numVertices] = vertex;

        // Percorrer todas as posições da matriz
        for (int i = 0; i <= numVertices; i++) {

            // A coluna do índice novo vértice é colocada a null. Significa que não existem ligações
            adjMatrix[numVertices][i] = null;

            // A linha do índice do novo vértice é colocada a null. Significa que não existem ligações
            adjMatrix[i][numVertices] = null;
        }

        // Incrementar o número de vértices
        numVertices++;
    }

    /**
     * Método que permite remover um vértice do grafo. Todas as ligações que
     * este vértice continha com outros vértices também são removidos
     *
     * @param vertex vertice a remover
     */
    @Override
    public void removeVertex(T vertex) {

        // Indice do vértice
        int indexVertex = getIndex(vertex);

        // Se o indice é válido, significa que o indice existe
        if (indexIsValid(indexVertex)) {
            throw new NoSuchElementException("O vértice não existe");
        }

        // Eliminar o vértice do array de vértices
        vertices[indexVertex] = null;

        // Shift dos vertices para a esquerda a partir do indice removido
        for (int i = indexVertex; i < numVertices - 1; i++) {
            vertices[i] = vertices[i + 1];
        }

        // Shift das linhas para cima a partir do indice removido
        for (int i = indexVertex; i < numVertices - 1; i++){
            for (int j = 0; j < numVertices; j++) {
                adjMatrix[i][j] = adjMatrix[i + 1][j];
            }
        }

        // Shift das colunas para a esquerda a partir do indice removido
        for (int i = indexVertex; i < numVertices - 1; i++){
            for (int j = 0; j < numVertices; j++) {
                adjMatrix[j][i] = adjMatrix[j][i + 1];
            }
        }

        // Decrementar o número de vértices
        numVertices--;

        for (int i = 0; i < vertices.length; i++) {
            adjMatrix[i][numVertices] = null;
            adjMatrix[numVertices][i] = null;
        }
    }

    /**
     * Método que permite adicionar uma aresta. Uma aresta é uma ligação entre
     * dois vértices existentes
     *
     * @param vertex1 vertice de uma ponta da aresta
     * @param vertex2 vertice da outra ponta da aresta
     */
    @Override
    public void addEdge(T vertex1, T vertex2) {

        // Obter os índices dos vértices
        int indexVertex1 = getIndex(vertex1);
        int indexVertex2 = getIndex(vertex2);

        // Se os dois indices forem válidos
        if (indexIsValid(indexVertex1) && indexIsValid(indexVertex2)) {

            // As posições contituídas pelos pontos passam a ser 1.
            // Significa que agora existe uma ligação entre os dois vértices
            adjMatrix[indexVertex1][indexVertex2] = new Double(0);

        } else {
            throw new NoSuchElementException("Algum dos vértices não existe");
        }
    }

    /**
     * Método que permite remover uma aresta entre dois vértices existentes
     *
     * @param vertex1 vertice de uma ponta da aresta a eliminar
     * @param vertex2 vertice da outra ponta da aresta a eliminar
     */
    @Override
    public void removeEdge(T vertex1, T vertex2) {

        // Indices dos vértices
        int indexVertex1 = getIndex(vertex1);
        int indexVertex2 = getIndex(vertex2);

        // Se os indices forem validos
        if (indexIsValid(indexVertex1) && indexIsValid(indexVertex2)) {

            // As posições constituídas pelos pontos passam a ser null
            // Significa que deixa de haver uma ligação entre os dois vértices
            adjMatrix[indexVertex1][indexVertex2] = null;
            adjMatrix[indexVertex2][indexVertex1] = null;

        } else {
            throw new NoSuchElementException("Algum dos vértices não existe");
        }
    }

    /**
     * Método que permite realizar uma travessia em largura a partir de um
     * determinado vértice existente
     *
     * @param startVertex vértice que dá início à travessia em largura
     * @return iterador que performa a travessia em largura
     */
    @Override
    public Iterator iteratorBFS(T startVertex) {

        // Guardar o indice do vertice
        Integer x;

        // Queue formada pelos índices dos vértices
        LinkedQueue<Integer> traversalQueue = new LinkedQueue<>();

        // Lista que irá receber o valor dos vértices
        DoubleLinkedUnorderedList<T> resultList = new DoubleLinkedUnorderedList<>();

        // Indice do vertice inicial
        int startIndex = getIndex(startVertex);

        // Se o indice nao for valido ( vertice nao existe )
        // Retorna um iterador vazio
        if (!indexIsValid(startIndex)) {
            return resultList.iterator();
        }

        // Array de boolean que irá verificar se os vértices já foram visitados
        boolean[] visited = new boolean[numVertices];

        // Inicializar o array com false
        for (int i = 0; i < visited.length; i++) {
            visited[i] = false;
        }

        // Adicionar o indice do primeiro vértice
        traversalQueue.enqueue(startIndex);

        // Sinalizar o indice do primeiro vértice como visitado
        visited[startIndex] = true;

        // Enquanto a queue tiver indices
        while (!traversalQueue.isEmpty()) {
            try {
                // Obter o indice da queue
                x = (Integer) traversalQueue.dequeue();

                // Adicionar o elemento do vértice indicado pelo indice
                resultList.addToRear(vertices[x]);

                // Encontrar todos os vértices adjacentes a x que não foram visitados
                for (int i = 0; i < numVertices; i++) {

                    // Se o vertice tiver ligação e ainda não foi visitado
                    if (adjMatrix[x][i] != null && !visited[i]) {

                        // Adicionar o indice à queue
                        traversalQueue.enqueue(i);

                        // Sinalizar o vértice como visitado
                        visited[i] = true;
                    }
                }
            } catch (EmptyCollectionException ex) {
                ex.printStackTrace();
            }
        }
        // Retornar o iterador
        return resultList.iterator();
    }

    /**
     * Método que permite realizar uma travessia em comprimento a partir de um
     * determinado vértice existente
     *
     * @param startVertex vértice que dá inicio à travessia em comprimento
     * @return iterador que performa a travessia em comprimento
     */
    @Override
    public Iterator iteratorDFS(T startVertex) {

        // Indice do vertice atual
        Integer x;

        // Sinalizar se o vértice foi encontrado
        boolean found;

        // Stack formada pelos índices dos vértices
        LinkedStack<Integer> traversalStack = new LinkedStack<>();

        // Lista que irá receber o valor dos vértices
        DoubleLinkedUnorderedList<T> resultList = new DoubleLinkedUnorderedList<>();

        // Indice do vertice inicial
        int startIndex = getIndex(startVertex);

        // Se o indice nao for valido ( vertice nao existe )
        // Retorna um iterador vazio
        if (!indexIsValid(startIndex)) {
            return resultList.iterator();
        }

        // Array de boolean que irá verificar se os vértices já foram visitados
        boolean[] visited = new boolean[numVertices];

        // Inicializar o array com false
        for (int i = 0; i < numVertices; i++) {
            visited[i] = false;
        }

        // Adicionar o indice do primeiro vértice
        traversalStack.push(startIndex);

        // Adicionar o elemento do vértice indicado pelo indice
        resultList.addToRear(vertices[startIndex]);

        // Sinalizar o indice do primeiro vértice como visitado
        visited[startIndex] = true;

        // Enquanto a stack tiver indices
        while (!traversalStack.isEmpty()) {
            // Obter o indice da stack
            x = traversalStack.peek();
            // Vértice ainda não encontrado
            found = false;

            // Encontrar o vertice adjacente a x que ainda não foi visitado
            for (int i = 0; (i < numVertices) && !found; i++) {

                // Se o vertice tiver ligação e ainda não foi visitado
                if (adjMatrix[x][i] != null && !visited[i]) {

                    // Adicionar o índice à stack
                    traversalStack.push(i);

                    // Adicionar o elemento do vértice indicado pelo indice
                    resultList.addToRear(vertices[i]);

                    // Sinalizar como visitado
                    visited[i] = true;

                    // Sinalizar como encontrado
                    found = true;
                }
            }
            // Se o vertice nao for encontrado e ainda houver indices na stack
            // Obtem o proximo indice da stack
            if (!found && !traversalStack.isEmpty()) {
                traversalStack.pop();
            }
        }

        // Retornar o iterador
        return resultList.iterator();
    }

    /**
     * Método que retorna um iterador que contém o caminho mais curto entre os dois vértices.
     *
     * @param startVertex vértice que dá inicio à travessia em comprimento
     * @param targetVertex vértice que coloca fim à travessia em comprimento
     * @return um iterador com o caminho mais curto entre os dois vértices
     */
    @Override
    public Iterator iteratorShortestPath(T startVertex, T targetVertex) {

        Iterator iterator = null;

        // obtém os indices dos vértices
        int source = this.getIndex(startVertex);
        int destination = this.getIndex(targetVertex);

        // se os dois indices forem válidos, significa que os vértices existem
        if (this.indexIsValid(source) && this.indexIsValid(destination)) {
            try {
                //aplica o algoritmo de dijkstra que indica o caminho mais curto entre os 2 vértices
                iterator = this.dijkstra(source, destination);
            } catch (EmptyCollectionException e) {
                e.printStackTrace();
            }
        }

        // retorna o iterador
        return iterator;
    }

    /**
     * Método que verifica se o grafo está vazio. Um grafo vazio é um grafo
     * que não tem vértices.
     *
     * @return true se está vazio ou false se possui vértices
     */
    @Override
    public boolean isEmpty() {
        return numVertices == 0;
    }

    /**
     * Método verifica se o grafo é conexo. Um grafo conexo é um grafo que para
     * quaisquer dois vértices existe um caminho entre eles
     *
     * @return true se for conexo ou false se não for
     */
    @Override
    public boolean isConnected() {

        // se o grafo estiver vazio, não é conexo, logo não existem vértices nem arestas. retorna false
        if (isEmpty()) {
            return false;
        }

        // Iterador com os vértices do grafo
        Iterator<T> iterator = iteratorBFS(vertices[0]);

        // Número de vértices obtidos do iterador
        int numberVerticesFromIterator = 0;

        // Verificar o número de vértices obtidos do iterador
        while (iterator.hasNext()) {
            iterator.next();
            numberVerticesFromIterator++;
        }

        // Se o número de ligações for diferente do número de vértices, significa que existe
        // algum vértice que não possui ligação com outros vértices existentes.
        return numberVerticesFromIterator == numVertices;
    }

    /**
     * Método que retorna o tamanho do grafo. O tamanho do grafo é o
     * número de vértices existentes no grafo
     *
     * @return o tamanho do grafo
     */
    @Override
    public int size() {
        return numVertices;
    }

    /**
     * Método que permite expandir a capacidade do grafo. Este método aumenta
     * para o dobro o tamanho da matriz de adjacências e o array de vértices
     */
    private void expandCapacity() {

        // Se o tamanho da matriz for igual ao número de vértices, expande-se a matrix existente
        if (adjMatrix.length == numVertices) {

            // Nova matriz de adjacências
            Double[][] newAdjMatrix = new Double[adjMatrix.length * 2][adjMatrix.length * 2];

            // Passar todos os valores para a nova matriz de adjacências
            for (int i = 0; i < adjMatrix.length; i++) {
                System.arraycopy(adjMatrix[i], 0, newAdjMatrix[i], 0, adjMatrix.length);
            }

            // Definir a nova matriz de adjacências
            this.adjMatrix = newAdjMatrix;
        }

        // Novo array de vertices
        T[] newVertices = (T[]) (new Object[vertices.length * 2]);

        // Passar todos os vértices para o novo array de vértices
        System.arraycopy(vertices, 0, newVertices, 0, vertices.length);

        // Definir o novo array de vértices
        this.vertices = newVertices;
    }

    /**
     * Método que obtém o índice do vertice inserido. Caso o vértice não exista
     * retorna -1
     *
     * @param vertex vertice a verificar
     * @return índice do vértice
     */
    protected int getIndex(T vertex) {

        // se o vértice for nulo
        if (vertex == null) {
            return -1;
        }

        // Percorrer o array de vértices
        for (int i = 0; i < vertices.length; i++) {

            // Se o vertice introduzido for igual ao vertice da posição atual
            if (vertex.equals(vertices[i])) {
                return i; // Retornar o index da posição atual
            }
        }

        // se o vértice não for encontrado, retorna -1
        return -1;
    }

    /**
     * Método que verifica se o indice é válido. Para ser válido tem de
     * ser maior que zero e menor ou igual que o tamanho do array de vertices
     *
     * @param index indice a verificar
     * @return true se for válido ou false se for inválido
     */
    protected boolean indexIsValid(int index) {
        return index >= 0 && index <= vertices.length;
    }

    /**
     * Método que retorna um iterador com o caminho mais curto.
     * Este método é calculado com a ajuda do algoritmo Dijkstra, que consiste
     *
     * @param origem vértice que dá inicio à travessia em comprimento
     * @param destino vértice que dá fim à travessia em comprimento
     * @return um iterador com caminho mais curto entre os dois vértices
     */
    private Iterator dijkstra(int origem, int destino) throws EmptyCollectionException {

        // lista para a distancia de cada vertice
        double[] distancia = new double[this.numVertices];

        // lista que vai guardar o melhor caminho
        Integer[] anterior = new Integer[this.numVertices];

        // definir todos os valores da lista a null
        for (int i = 0; i < this.numVertices; i++) {
            anterior[i] = null;
        }

        // lista que marca os vertices visitados
        boolean[] vertices_visitados = new boolean[this.numVertices];

        for (int i = 0; i < this.numVertices; i++) {

            // define a distância de cada vertice com valor infinito
            distancia[i] = Double.MAX_VALUE;

            // define todos os vertices como não usados
            vertices_visitados[i] = false;
        }

        // distância do vertice de origem
        distancia[origem] = 0;

        for (int i = 0; i < this.numVertices - 1; i++) {

            // valor de menor custo
            int vertice_menorCusto = minDistance(distancia, vertices_visitados);

            // coloca o vertice de menor custo como usado
            vertices_visitados[vertice_menorCusto] = true;

            for (int j = 0; j < this.numVertices; j++) {

                // se o vertice ainda noa tiver sido visitado,
                if (!vertices_visitados[j] && this.adjMatrix[vertice_menorCusto][j] != null && distancia[vertice_menorCusto] != Double.MAX_VALUE
                        && distancia[vertice_menorCusto] + this.adjMatrix[vertice_menorCusto][j] < distancia[j]) {

                    distancia[j] = distancia[vertice_menorCusto] + this.adjMatrix[vertice_menorCusto][j];

                    anterior[j] = vertice_menorCusto;
                }
            }
        }

        // lista que irá receber o valor dos vértices
        ArrayUnorderedList<T> resultList = new ArrayUnorderedList<>();
        Integer target = destino;


        if (anterior[target] != null) {
            while (target != null){
                resultList.addToFront(this.vertices[target]);
                target = anterior[target];
            }
        }

        // retorna o iterador
        return resultList.iterator();
    }

    /**
     * Calcula o caminho de menor custo entre os vértices.
     *
     * @param distancias distância entre os vértices
     * @param vertices_visitados vértice usado
     * @return valor do menor custo
     */
    protected int minDistance(double distancias[], boolean vertices_visitados[]) {

        // valor do caminho mais curto
        double minDistance = Double.MAX_VALUE;

        // indice do caminho mais curto
        int minDistanceIndex = -1;

        for (int i = 0; i < this.numVertices; i++) {

            // se a distancia do vertice for menor que o valor do caminho mais curto e o vertice não tiver sido usado
            if (vertices_visitados[i] == false && distancias[i] <= minDistance) {

                // a distancia passa a ser o valor do caminho mais curto
                minDistance = distancias[i];

                // guarda o valor do indice
                minDistanceIndex = i;
            }
        }

        // retorna o valor de menor custo
        return minDistanceIndex;
    }

    /**
     * Retorna uma representação da matriz
     *
     * @return uma representação da matriz
     */
    @Override
    public String toString() {
        String str = "";
        for (int i = 0; i < this.numVertices; i++) {
            str += this.vertices[i] + "   ";
            for (int j = 0; j < this.numVertices; j++) {
                str += "[" + this.adjMatrix[i][j] +"] ";
            }
            str += "\n ";
        }
        return str;
    }

}
