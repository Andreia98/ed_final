package Interfaces;

import ED_Ficha5.ArrayUnorderedList;
import Exceptions.EmptyCollectionException;
import Exceptions.ShortestPathException;
import Models.Utilizador;

import javax.swing.*;
import java.util.Iterator;

public interface GestorADT {

    /**
     * Método responsável por ler o mapa
     *
     * @param filepath caminho para o ficheiro do mapa
     * @return true ou false
     */
    public boolean readMapa(String filepath);

    /**
     * Método responsável por ler o ficheiro CSV das tabelas classificativas
     */
    public void readCSVFile();

    /**
     * Método responsável por calcular o custo do caminho mais curto
     *
     * @param level numero inteiro referente ao nivel de dificuldade escolhido
     * @return um Double referente valor do custo
     */
    public Double CustoDoCaminhoMaisCurto(int level) throws ShortestPathException;

    /**
     * Método responsável por retornar o iterador do caminho com menos custo e mais curto
     *
     * @param level numero inteiro referente ao nivel de dificuldade escolhido
     * @return iterador do caminho mais curto
     */
    public Iterator CaminhoMaisCurto_MenosCusto(int level);

    /**
     * Método responsável por criar o grafo do respetivo mapa
     */
    public void Criar_Grafo();

    /**
     * Método responsável por criar uma frame para a demonstração do mapa
     */
    public void verMapa();

    /**
     * Método responsável por realizar o jogo
     *
     * @param nome_player nome do jogador
     * @param nome_mapa   nome do mapa
     */
    public void iniciarJogo(String nome_player, String nome_mapa);

    /**
     * Método responsável por adicionar à frame do jogo os buttons com as ligações existentes
     * para a atual posição
     *
     * @param ligacoes       ligações para o aponseto
     * @param frameJogo      frame do jogo
     * @param player         nome do player
     * @param nome_mapa      nome do mapa
     * @param posicao_actual posição atual do jogador
     */
    public void criarButoesLigacoes(String[] ligacoes, JFrame frameJogo, String player, String nome_mapa, String posicao_actual);

    /**
     * Método responsável por ordenar a tabela de classificações
     *
     * @return um ArrayUnorderList com os users ordenados
     * @throws EmptyCollectionException se não existirem users nas classificações
     */
    public ArrayUnorderedList<Utilizador> ordenar_tabela_classificativa() throws EmptyCollectionException;
}
