/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import Exceptions.ShortestPathException;

/**
 * Interface que define uma rede. Uma rede é um grafo pesado
 *
 * @param <T>
 * @author Pedro
 */
public interface NetworkADT<T> extends GraphADT<T> {

    /**
     * Insere uma aresta entre dois vertices do grafo
     *
     * @param vertice1 primeiro vertice
     * @param vertice2 segundo vertice
     * @param peso  o peso
     */
    public void addEdge(T vertice1, T vertice2, double peso);

    /**
     * Retorna o custo do caminho mais pequeno entre dois vertices
     *
     * @param vertice1 primeiro vertice
     * @param vertice2 segundo vertice
     * @return o custo do caminho mais curto neste grafo
     */
    public double shortestPathWeight(T vertice1, T vertice2) throws ShortestPathException;

}
