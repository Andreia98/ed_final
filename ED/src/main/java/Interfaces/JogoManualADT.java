package Interfaces;


import ED_Ficha5.ArrayUnorderedList;
import Exceptions.ConnectionNotFoundException;

public interface JogoManualADT<T> extends GraphADT<T> {

    /**
     * Método responsável por retornar um array UnorderList com as ligações para a posição actual
     * @return ArrayUnorderedList com as ligações para a posição actual
     */
    public ArrayUnorderedList<T> getLigacoesAposento();

    /**
     * Método responsável por alterar a posição atual do utilizador para a nova posição e
     * por alterar a vida.
     * @param next_position nova posição
     * @return True quando a posição actual for alterada
     * @throws ConnectionNotFoundException
     */
    public boolean changePosition(T next_position) throws ConnectionNotFoundException;

    /**
     * Método responsável por retornar se o jogador está ou não vivo
     * @return true se a vida for maior que 0
     */
    public boolean isAlive();

    /**
     * Método responsável por multiplicar o dano conforme o nivel selecionado
     *
     * @param level nivel escolhido pelo utilizador
     */
    public void dificuldade(int level);

    /**
     * Método responsável por retornar se o jogo está ou não completo
     * @return true se a posição actual do jogador for igual à posição final
     */
    public boolean isComplete();

}
