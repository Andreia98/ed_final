
import DoubleLinkedList.DoubleLinkedOrderedList;
import ED_Ficha5.ArrayUnorderedList;
import Exceptions.ConnectionNotFoundException;
import Exceptions.EmptyCollectionException;
import Exceptions.ShortestPathException;
import Graph.JogoManual;
import Interfaces.GestorADT;
import Models.Aposento;
import Models.Mapa;
import Models.Utilizador;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import javax.sound.sampled.*;
import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.Iterator;
import java.util.Scanner;

public class Gestor implements GestorADT {
    private Mapa mapa = null;
    private ArrayUnorderedList<Utilizador> users = null;
    private JogoManual<String> jogoManual = null;
    private JFrame frame = new JFrame();
    private Clip clip;

    public Gestor() {
    }

    /**
     * Método responsável por ler o mapa
     *
     * @param filepath caminho para o ficheiro do mapa
     * @return true ou false
     */
    public boolean readMapa(String filepath) {
        JSONParser parser = new JSONParser();
        try (Reader reader = new FileReader(filepath)) {
            JSONObject jsonObject = (JSONObject) parser.parse(reader);

            int fantasma;
            Aposento[] aposentos;
            String nome_aposento;
            int j = 0;

            //nome do mapa
            String nome = (String) jsonObject.get("nome");
            //pontos do mapa
            int pontos = (int) (long) jsonObject.get("pontos");
            //array Mapa
            JSONArray mapArray = (JSONArray) jsonObject.get("mapa");
            //iterator do array mapa
            Iterator mapa_iterator = mapArray.iterator();
            //inicializa o array de aposentos com o tamanho do mapArray
            aposentos = new Aposento[mapArray.size()];

            while (mapa_iterator.hasNext()) {
                JSONObject v = (JSONObject) mapa_iterator.next();
                //nome do aposento
                nome_aposento = (String) v.get("aposento");
                //fantasma do aposento
                fantasma = (int) (long) v.get("fantasma");
                //array ligações do aposento
                JSONArray ligacoesArray = (JSONArray) v.get("ligacoes");
                //iterator do array de ligações
                Iterator ligacoes_iterator = ligacoesArray.iterator();
                //inicializa um array temp de ligações , com o tamanho de ligacoesArray
                ArrayUnorderedList<String> ligacoes = new ArrayUnorderedList();
                //guarda as ligações do aposento no array temp
                for (int i = 0; i < ligacoesArray.size(); i++) {
                    String ligacao = (String) ligacoesArray.get(i);
                    if (ligacoes.isEmpty()) {
                        ligacoes.addToFront(ligacao);
                    } else {
                        ligacoes.addToRear(ligacao);
                    }
                }
                //guarda os aposentos todos
                aposentos[j] = new Aposento(nome_aposento, fantasma, ligacoes);
                j++;
            }
            //criação do mapa
            mapa = new Mapa(nome, pontos, aposentos);
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("Não existe o ficheiro");
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Erro");
            return false;
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Método para o menu Inicial
     */
    public void menuInicial() throws IOException {
        frame = new JFrame();
        frame.setTitle("Casa Assombrada Demo");
        frame.setLayout(new GridBagLayout());

        JButton novoJogo_Button = new javax.swing.JButton();
        JButton classfic_button = new javax.swing.JButton();

        frame.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        JLabel jcontantPanel = new JLabel();
        jcontantPanel.setIcon(new ImageIcon("Files/2019hauntedhouse.jpg"));

        jcontantPanel.setLayout(new GridBagLayout());
        GridBagConstraints gdc = new GridBagConstraints();
        //botão para aceder ao menu de novo jogo
        novoJogo_Button.setText("Novo Jogo");

        novoJogo_Button.addActionListener(actionEvent -> {
            MenuIniciarJogo();
            frame.dispose();
        });

        gdc.gridx = 1;
        gdc.gridy = 2;
        gdc.insets = new Insets(0, 25, 20, 25);
        novoJogo_Button.setFont(new Font("Arial", Font.BOLD, 17));
        jcontantPanel.add(novoJogo_Button, gdc);

        //Buttão para aceder a classificações
        classfic_button.setText("Classificacoes");
        classfic_button.addActionListener(actionEvent -> {
            try {
                tabela_classificacoes();
            } catch (EmptyCollectionException e) {
                e.printStackTrace();
            }
        });
        gdc.gridx = 1;
        gdc.gridy = 3;
        gdc.insets = new Insets(0, 25, 20, 25);
        classfic_button.setFont(new Font("Arial", Font.BOLD, 17));
        jcontantPanel.add(classfic_button, gdc);

        frame.add(jcontantPanel);
        frame.setResizable(false);
        frame.pack();
        frame.setVisible(true);
    }

    /**
     * Método responsável por imprimir a tabela classificativa
     */
    private void tabela_classificacoes() throws EmptyCollectionException {
        frame.getContentPane().removeAll();
        frame.repaint();
        frame.setTitle("Classificacoes");

        frame.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        JLabel mapa = new JLabel("Mapa:");
        mapa.setOpaque(true);
        mapa.setBackground(Color.lightGray);
        gbc.gridx = 0;
        gbc.gridy = 0;
        panel.add(mapa, gbc);

        JComboBox mapa_comboBox = new JComboBox();

        String[] mapas = getMapas();
        //define o model da comboBox , com os mapas da pasta Files
        mapa_comboBox.setModel(new DefaultComboBoxModel<>(mapas));

        gbc.gridy = 0;
        gbc.gridx = 1;
        gbc.insets = new Insets(0, 10, 0, 0);
        panel.add(mapa_comboBox, gbc);

        panel.setBackground(Color.LIGHT_GRAY);
        gbc.gridx = 0;
        gbc.gridy = 0;

        frame.add(panel, gbc);
        //recebe o index do mapa escolhido

        JButton ver_button = new JButton("Ver Classificacoes");
        ver_button.addActionListener(evt -> {
            try {
                int mapa_index = mapa_comboBox.getSelectedIndex();
                String nome_mapa_classification = mapas[mapa_index];

                janela_classificacoes(frame, nome_mapa_classification);
            } catch (EmptyCollectionException e) {
                e.printStackTrace();
            }
        });
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.insets = new Insets(50, 50, 50, 50);
        frame.add(ver_button, gbc);

        frame.getContentPane().setBackground(Color.lightGray);
        frame.setSize(400, 400);
        frame.setVisible(true);
    }

    /**
     * Método responsável por retornar todos os mapas existentes na pasta mapas
     *
     * @return Array do tipo String com os mapas
     */
    private String[] getMapas() {
        String[] mapas = new String[0];
        File pasta = new File("Mapas/");
        //Corre a pasta Files, e procura por files do tipo json
        for (File file : pasta.listFiles()) {
            if (file.getName().indexOf(".json") > 0) {
                String[] temp = new String[mapas.length + 1];
                System.arraycopy(mapas, 0, temp, 0, mapas.length);
                temp[mapas.length] = file.getName();
                mapas = temp;
            }
        }
        return mapas;
    }

    /**
     * Método responsável por criar a janela com as classificações para um determinado mapa
     *
     * @param frame     frame da janela
     * @param nome_mapa nome do mapa
     * @throws EmptyCollectionException caso não existam exceções retorna exceção
     */
    private void janela_classificacoes(JFrame frame, String nome_mapa) throws EmptyCollectionException {
        frame.getContentPane().removeAll();
        frame.repaint();
        frame.setTitle("Classificacoes");
        frame.setLayout(new GridBagLayout());

        JPanel tabela_panel = new JPanel();
        JLabel text = new JLabel();
        JPanel button_panel = new JPanel();
        String str = classificacoes_output(nome_mapa);
        text.setText(str);
        tabela_panel.add(text);

        JButton backtoMenu = new JButton("Voltar");
        backtoMenu.addActionListener(evt -> {
            try {
                menuInicial();
                frame.dispose();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        button_panel.add(backtoMenu);

        JScrollPane scrollPanel = new JScrollPane(tabela_panel, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollPanel.setPreferredSize(new Dimension(500, 500));

        frame.add(scrollPanel);
        frame.add(button_panel);
        frame.pack();
        frame.setVisible(true);
    }

    /**
     * Método responsável por retornar uma String no formato Html para exibir as classificações
     *
     * @param nome_mapa_classification nome do mapa escolhido
     * @return uma String no formato HTML
     * @throws EmptyCollectionException caso não existam classificações
     */
    private String classificacoes_output(String nome_mapa_classification) throws EmptyCollectionException {
        ArrayUnorderedList<Utilizador> utilizadores = ordenar_tabela_classificativa();
        Iterator utilizadoresIterator = utilizadores.iterator();

        String str = "<html> <style> " +
                "table {" +
                "  font-family: arial, sans-serif;" +
                "  border-collapse: collapse;" +
                "  width: 100%n" +
                "}" +
                "" +
                "td, th {" +
                "  border: 1px solid #dddddd;" +
                "  text-align: left;" +
                "  padding: 8px;" +
                "}" +
                "" +
                "tr:nth-child(even) {" +
                "  background-color: #dddddd;" +
                "}" +
                "</style> " +
                "<Table>" +
                "< <tr>" +
                "    <th>Nome</th>" +
                "    <th>Pontos</th>" +
                "    <th>Mapa</th>" +
                "<th>Caminhos</th>" +
                "  </tr>";

        while (utilizadoresIterator.hasNext()) {
            Utilizador user = (Utilizador) utilizadoresIterator.next();
            if (nome_mapa_classification.equals(user.getMapa())) {
                str += "<tr> <th>" + user.getNome() + "</th>" + "<th>" + user.getPontos() + "</th>" + "<th>" + user.getMapa() + "</th>" + "<th>" + user.getCaminhos() + "</th> </tr>";
            }
        }
        str += "</Table> </html>";

        return str;
    }

    /**
     * Método responsável por adicionar um jogador à tabela classificativa
     *
     * @param user utilizador a ser adicionado
     * @return true or false
     */
    private boolean AddToClassification(Utilizador user) {
        readCSVFile();
        File file = new File("Files/Classificacoes.csv");

        try {
            users.addToRear(user);
            //Escrever no ficheiro
            String str = "Nome,Pontos,Mapa,Caminhos\n";
            FileWriter fw = new FileWriter(file);

            for (Utilizador utilizador : users) {
                str += utilizador.getNome() + "," + utilizador.getPontos() + "," + utilizador.getMapa() + "," + utilizador.getCaminhos() + "\n";
            }
            fw.write(str);
            fw.close();
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Método responsável por ler o ficheiro CSV das tabelas classificativas
     */
    public void readCSVFile() {
        File file = new File("Files/Classificacoes.csv");

        try {
            Scanner reader = new Scanner(file);
            reader.nextLine();//passa header a frente
            users = new ArrayUnorderedList<>();

            while (reader.hasNextLine()) {
                String data = reader.nextLine();
                String[] dados = data.split(",");
                users.addToRear(new Utilizador(dados[0], Integer.parseInt(dados[1]), dados[2], Integer.parseInt(dados[3])));
            }
            reader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Menu para Iniciar o jogo
     */
    private void MenuIniciarJogo() {
        JLabel label_nomeJogador = new javax.swing.JLabel();
        JFormattedTextField nomejogador_text = new javax.swing.JFormattedTextField();
        JLabel mapa_jlabel = new javax.swing.JLabel();
        JLabel label_dificuldade = new javax.swing.JLabel();
        JComboBox dificuldade_comboBox = new javax.swing.JComboBox<>();
        JComboBox mapa_comboBox = new javax.swing.JComboBox<>();
        JButton iniciarJogo_buton = new javax.swing.JButton();
        JButton simulacao_button = new JButton();
        JButton ver_mapa = new JButton();
        JButton voltar = new JButton("Voltar");

        JFrame frameJogo = new JFrame();
        frameJogo.setTitle("Novo Jogo");

        //Nome Jogador
        label_nomeJogador.setText("Nome Jogador:");
        nomejogador_text.setHorizontalAlignment(JTextField.LEFT);

        //Label para Mapa
        mapa_jlabel.setText("Mapa:");

        //Label para dificuldade
        label_dificuldade.setText("Dificuldade:");

        //model para diculdade com as possibilidades
        dificuldade_comboBox.setModel(new javax.swing.DefaultComboBoxModel<>(
                new String[]{"Facil", "Medio", "Dificil"}));

        //array responsavel por guardar as strings dos mapas existentes
        String[] mapas = getMapas();

        //define o model da comboBox , com os mapas da pasta Files
        mapa_comboBox.setModel(new DefaultComboBoxModel<>(mapas));

        simulacao_button.setText("Iniciar Simulacao");
        simulacao_button.addActionListener(actionEvent -> {
            int mapa = mapa_comboBox.getSelectedIndex();
            //recebe o nome do mapa
            String nome_mapa = "Mapas/" + mapas[mapa];
            //lê o mapa selecionado
            readMapa(nome_mapa);
            //recebe o index referente ao nivel de dificuldade selecionado
            int level = dificuldade_comboBox.getSelectedIndex() + 1;
            //recebe o custo do caminho mais curto
            Double doublecusto = null;
            try {
                doublecusto = CustoDoCaminhoMaisCurto(level);
            } catch (ShortestPathException e) {
                e.printStackTrace();
            }
            //recebe o iterador do caminho mais curto
            Iterator iterator1 = CaminhoMaisCurto_MenosCusto(level);
            //string para guardar o caminho
            String str = "<HTML>";
            //percorre o iterador e acrescenta o aposento à string
            while (iterator1.hasNext()) {
                str += iterator1.next() + "<font color='red'>--></font>";
            }
            //Cria uma joptionPane onde mostra ao utilizador o resultado da simulação
            JOptionPane.showMessageDialog(frameJogo,
                    str + "\n" + "Vida Perdida:" + doublecusto,
                    "Resultado Simulacao",
                    JOptionPane.PLAIN_MESSAGE);

        });
        //define o texto do butão iniciar jogo
        iniciarJogo_buton.setText("Iniciar jogo");
        iniciarJogo_buton.addActionListener(evt -> {
            //recebe o nome do jogador
            if (nomejogador_text.getText().isEmpty()) {
                JOptionPane.showMessageDialog(frameJogo, "Escolha um nome antes de iniciar",
                        "Erro", JOptionPane.ERROR_MESSAGE);
                MenuIniciarJogo();
                frameJogo.dispose();
            } else {
                String nome = nomejogador_text.getText();

                //recebe o index do mapa escolhido
                int mapa = mapa_comboBox.getSelectedIndex();

                //recebe o nome do mapa
                String nome_mapa = "Mapas/" + mapas[mapa];

                String nomeMapa = mapas[mapa];
                //lê o mapa selecionado
                readMapa(nome_mapa);

                Criar_Grafo();

                int level = dificuldade_comboBox.getSelectedIndex() + 1;

                //define a dificuldade
                jogoManual.dificuldade(level);

                //inicia o jogo
                iniciarJogo(nome, nomeMapa);
                frameJogo.dispose();
            }
            frameJogo.setResizable(false);
        });
        ver_mapa.setText("Ver Mapa");
        ver_mapa.addActionListener(evt -> {
            int mapa = mapa_comboBox.getSelectedIndex();

            //recebe o nome do mapa
            String nome_mapa = "Mapas/" + mapas[mapa];
            readMapa(nome_mapa);
            Criar_Grafo();
            verMapa();
        });
        voltar.addActionListener(evt -> {
            try {
                menuInicial();
            } catch (IOException e) {
                e.printStackTrace();
            }
            frameJogo.dispose();
        });
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(frameJogo.getContentPane());
        frameJogo.getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(21, 21, 21)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(label_nomeJogador)
                                                        .addComponent(label_dificuldade)
                                                        .addComponent(mapa_jlabel))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                        .addComponent(nomejogador_text)
                                                        .addComponent(dificuldade_comboBox, 0, 91, Short.MAX_VALUE)
                                                        .addComponent(mapa_comboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(61, 61, 61)
                                                .addComponent(iniciarJogo_buton)
                                        )
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(40, 40, 40)
                                                .addComponent(simulacao_button))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(65, 65, 65)
                                                .addComponent(ver_mapa))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(70, 70, 70)
                                                .addComponent(voltar))
                                )
                                .addContainerGap(29, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(33, 33, 33)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(label_nomeJogador)
                                        .addComponent(nomejogador_text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(label_dificuldade)
                                        .addComponent(dificuldade_comboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(16, 16, 16)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(mapa_jlabel)
                                        .addComponent(mapa_comboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(33, 33, 33)
                                .addComponent(iniciarJogo_buton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(ver_mapa)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(simulacao_button)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(voltar)
                                .addContainerGap(26, Short.MAX_VALUE))
        );
        frameJogo.setDefaultCloseOperation(0);
        frameJogo.pack();
        frameJogo.getContentPane().setBackground(Color.lightGray);
        frameJogo.setVisible(true);
    }

    /**
     * Método responsável por calcular o custo do caminho mais curto
     *
     * @param level numero inteiro referente ao nivel de dificuldade escolhido
     * @return um Double referente valor do custo
     */
    public Double CustoDoCaminhoMaisCurto(int level) throws ShortestPathException {
        Criar_Grafo();

        jogoManual.dificuldade(level);

        return jogoManual.shortestPathWeight("entrada", "exterior");
    }

    /**
     * Método responsável por retornar o iterador do caminho com menos custo e mais curto
     *
     * @param level numero inteiro referente ao nivel de dificuldade escolhido
     * @return iterador do caminho mais curto
     */
    public Iterator CaminhoMaisCurto_MenosCusto(int level) {
        Criar_Grafo();

        jogoManual.dificuldade(level);

        return jogoManual.iteratorShortestPath("entrada", "exterior");
    }

    /**
     * Método responsável por criar o grafo do respetivo mapa
     */
    public void Criar_Grafo() {
        jogoManual = new JogoManual<String>();

        jogoManual.addVertex("entrada");
        jogoManual.setPosicao_inicial("entrada");
        jogoManual.setPosicao_actual("entrada");

        for (int i = 0; i < mapa.getAposentos().length; i++) {
            jogoManual.addVertex(mapa.getAposentos()[i].getNome());
        }
        jogoManual.addVertex("exterior");
        jogoManual.setPosicao_final("exterior");

        for (int i = 0; i < mapa.getAposentos().length; i++) {
            Iterator iterador = mapa.getAposentos()[i].getLigacoes();

            for (int j = 0; iterador.hasNext(); j++) {

                String aposento = (String) iterador.next();

                if (aposento.equals("exterior")) {
                    jogoManual.addEdge(mapa.getAposentos()[i].getNome(), aposento);
                } else {
                    jogoManual.addEdge(mapa.getAposentos()[i].getNome(), aposento, mapa.getAposentos()[i].getFantasma());
                }
            }
        }
        jogoManual.setVida(mapa.getPontos());
    }

    /**
     * Método responsável por adicionar à frame do jogo os buttons com as ligações existentes
     * para a atual posição
     *
     * @param ligacoes       ligações para o aponseto
     * @param frameJogo      frame do jogo
     * @param player         nome do player
     * @param nome_mapa      nome do mapa
     * @param posicao_actual posição atual do jogador
     */
    public void criarButoesLigacoes(String[] ligacoes, JFrame frameJogo, String player, String nome_mapa, String posicao_actual) {
        frameJogo.getContentPane().removeAll();
        frameJogo.repaint();
        frameJogo.setTitle("Jogo");

        JLabel jcontentGlobal = new JLabel();
        JPanel jbuttons = new JPanel();
        JPanel jcontentpanelfantasma = new JPanel();

        GridBagConstraints gridBagConstraints = new GridBagConstraints();

        int j = 0;

        jbuttons.setLayout(new GridBagLayout());
        jcontentGlobal.setLayout(new GridBagLayout());
        jcontentGlobal.setIcon(new ImageIcon("Files/2019hauntedhouse.jpg"));

        File pasta = new File("Files/");
        //Corre a pasta Files, e procura por files do tipo json
        for (File file : pasta.listFiles()) {
            if (file.getName().equals(jogoManual.getPosicao_actual() + ".jpg")) {
                jcontentGlobal.setIcon(new ImageIcon("Files/" + jogoManual.getPosicao_actual() + ".jpg"));
            }
        }
        for (int i = 0; i < mapa.getAposentos().length; i++) {
            if (mapa.getAposentos()[i].getNome().equals(posicao_actual)) {
                if (mapa.getAposentos()[i].getFantasma() > 0) {
                    JLabel fantasma = new JLabel(new ImageIcon("Files/fantasma.png"));
                    fantasma.setBackground(new Color(0, 0, 0, 0));
                    gridBagConstraints.gridy = 0;
                    gridBagConstraints.gridx = 0;
                    jcontentpanelfantasma.setBackground(new Color(0, 0, 0, 0));
                    jcontentpanelfantasma.add(fantasma, gridBagConstraints);
                }
            }
        }

        for (String aposento : ligacoes) {
            JButton button = new JButton(aposento);
            button.addActionListener(actionEvent ->
            {
                try {
                    jogoManual.changePosition(aposento);
                    iniciarJogo(player, nome_mapa);
                } catch (ConnectionNotFoundException e) {
                    e.printStackTrace();
                }
            });
            j++;
            gridBagConstraints.gridx = j;
            gridBagConstraints.gridy = 1;
            gridBagConstraints.insets = new Insets(0, 25, 25, 25);

            if (aposento.equals("exterior")) {
                button.setForeground(Color.cyan);
            }
            jbuttons.add(button, gridBagConstraints);
        }
        jbuttons.setBackground(new Color(0, 0, 0, 0));
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridx = 0;
        jcontentGlobal.add(jcontentpanelfantasma, gridBagConstraints);

        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        jcontentGlobal.add(jbuttons, gridBagConstraints);

        jcontentGlobal.setBackground(new Color(0, 0, 0, 0));

        frameJogo.getContentPane().add(jcontentGlobal);

        frameJogo.setResizable(false);
        frameJogo.setVisible(true);
    }

    /**
     * Inicia um clip de uma musica
     */
    private void startMusic() {
        try {
            clip = AudioSystem.getClip();
            AudioInputStream ais = AudioSystem.getAudioInputStream(new File("files/Morreu.wav")); //ficheiro de musica
            clip.open(ais);
            clip.start();
            clip.loop(Clip.LOOP_CONTINUOUSLY); //toca a musica em loop ate o programa acabar
        } catch (LineUnavailableException | IOException | UnsupportedAudioFileException e) {
            System.out.println("Erro no metodo startMusic()");
        }
    }

    /**
     * Método responsável por realizar o jogo
     *
     * @param nome_player nome do jogador
     * @param nome_mapa   nome do mapa
     */
    public void iniciarJogo(String nome_player, String nome_mapa) {
        frame.setTitle("Jogo");
        ArrayUnorderedList<String> ligacoes;

        if (!jogoManual.isComplete() && jogoManual.isAlive()) {
            ligacoes = jogoManual.getLigacoesAposento();

            Iterator iterator = ligacoes.iterator();
            String[] ligacoesAposento = new String[0];

            for (int i = 0; iterator.hasNext(); i++) {
                String[] temp = new String[ligacoesAposento.length + 1];
                System.arraycopy(ligacoesAposento, 0, temp, 0, ligacoesAposento.length);
                temp[ligacoesAposento.length] = iterator.next().toString();
                ligacoesAposento = temp;
            }
            String posicao_actual = jogoManual.getPosicao_actual();
            criarButoesLigacoes(ligacoesAposento, frame, nome_player, nome_mapa, posicao_actual);
        }
        if (jogoManual.getVida() <= 0) {
            frame.getContentPane().removeAll();
            frame.repaint();
            jogoManual.setVida(0);
            frame.setLayout(new BorderLayout());

            JLabel died_label = new JLabel();
            died_label.setLayout(new GridBagLayout());

            died_label.setIcon(new ImageIcon("Files/youdied.gif"));

            JPanel contentPanel = new JPanel();
            contentPanel.setLayout(new GridBagLayout());

            GridBagConstraints gbc = new GridBagConstraints();
            JButton voltar = new JButton("Voltar para Menu");
            voltar.addActionListener(evt -> {
                try {
                    clip.stop();
                    frame.dispose();
                    menuInicial();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            gbc.gridx = 0;
            gbc.gridy = 1;

            contentPanel.add(voltar, gbc);
            contentPanel.setBackground(new Color(0, 0, 0, 0));

            frame.setTitle("Jogo");

            JLabel label = new JLabel("<html> <font color='red'>Morreste :( </font> </html>");
            label.setFont(new Font("Arial", Font.BOLD, 22));
            label.setBackground(new Color(0, 0, 0, 0));
            gbc.gridx = 0;
            gbc.gridy = 1;
            contentPanel.add(label);

            died_label.add(contentPanel);

            //inicia a musica quando morrer
            startMusic();

            frame.add(died_label, BorderLayout.CENTER);
            frame.pack();
            frame.setVisible(true);
            frame.setResizable(false);

        } else if (jogoManual.isComplete()) {
            frame.getContentPane().removeAll();
            frame.repaint();
            frame.setLayout(new GridBagLayout());
            frame.setTitle("Parabens");

            JLabel labelcongratz = new JLabel();
            labelcongratz.setLayout(new GridBagLayout());
            labelcongratz.setIcon(new ImageIcon("Files/congrats.gif"));

            labelcongratz.setLayout(new GridBagLayout());
            JLabel label = new JLabel("<html> <font color='yellow'> Total Pontos :" + jogoManual.getVida() + "</font> </html>");
            label.setFont(new Font("Arial", Font.BOLD, 25));
            label.setBackground(new Color(0, 0, 0, 0));

            GridBagConstraints gbc = new GridBagConstraints();
            //cria um button para voltar ao menu
            JButton button = new JButton("Voltar para o Menu");
            button.addActionListener(evt -> {
                try {
                    frame.dispose();
                    menuInicial();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            //define a posição da label
            gbc.gridx=0;
            gbc.gridy=0;
            labelcongratz.add(label,gbc);

            //define a posição do button
            gbc.gridy=1;
            gbc.gridx=0;
            gbc.insets = new Insets(75,75,75,75);
            labelcongratz.add(button,gbc);

            //adiciona o user à tabela classificativa no fim do jogo
            AddToClassification(new Utilizador(nome_player, jogoManual.getVida(), nome_mapa, jogoManual.getCount()));

            //imprime a matriz do jogo
            System.out.println(jogoManual.toString());

            frame.add(labelcongratz);
            frame.pack();
            frame.setSize(700, 400);
            frame.setVisible(true);
        }
        frame.setSize(700, 400);
        frame.setVisible(true);
    }

    /**
     * Método responsável por criar uma frame para a demonstração do mapa
     */
    public void verMapa() {
        Frame frameVerMapa = new JFrame();
        frameVerMapa.setTitle("Mapa");
        String str = "<html> <h2><font color='red'>Caminhos Disponiveis:</font></h2>";
        str += jogoManual.printGraph();

        JLabel textlabel = new JLabel();

        textlabel.setOpaque(true);
        textlabel.setText(str);
        JScrollPane scrollPanel = new JScrollPane(textlabel, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollPanel.setPreferredSize(new Dimension(400, 400));

        frameVerMapa.add(scrollPanel);
        frameVerMapa.pack();
        frameVerMapa.setVisible(true);
    }

    /**
     * Método responsável por ordenar a tabela de classificações
     *
     * @return um ArrayUnorderList com os users ordenados
     * @throws EmptyCollectionException se não existirem users nas classificações
     */
    public ArrayUnorderedList<Utilizador> ordenar_tabela_classificativa() throws EmptyCollectionException {
        ArrayUnorderedList<Utilizador> tabela = new ArrayUnorderedList<>();
        DoubleLinkedOrderedList<Integer> pontos = new DoubleLinkedOrderedList<>();

        readCSVFile();

        Iterator usersIterator = users.iterator();
        while (usersIterator.hasNext()) {
            Utilizador user = (Utilizador) usersIterator.next();
            pontos.add(user.getPontos());
        }
        ArrayUnorderedList<Integer> contemNaTabela = new ArrayUnorderedList<>();

        Utilizador[] utilizadores;

        Iterator iterator = users.iterator();
        int j = 0;
        Utilizador[] temp = new Utilizador[users.size()];
        while (iterator.hasNext()) {
            Utilizador user = (Utilizador) iterator.next();
            temp[j] = user;
            j++;
        }
        utilizadores = temp;

        for (int i = 0; i < utilizadores.length; ) {
            if (!contemNaTabela.contains(i) && pontos.last() == utilizadores[i].getPontos()) {
                contemNaTabela.addToRear(i);
                pontos.removeLast();

                tabela.addToRear(utilizadores[i]);
                i = 0;
            } else {
                i++;
            }
        }
        return tabela;
    }
}
