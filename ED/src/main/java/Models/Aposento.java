package Models;

import ED_Ficha5.ArrayUnorderedList;
import java.util.Iterator;

public class Aposento {

    private String nome;
    private int fantasma;
    private ArrayUnorderedList<String> ligacoes;

    public Aposento(String nome, int fantasma, ArrayUnorderedList<String> ligacoes) {
        this.nome = nome;
        this.fantasma = fantasma;
        this.ligacoes = ligacoes;
    }

    public int getFantasma() {
        return fantasma;
    }

    public void setFantasma(int fantasma) {
        this.fantasma = fantasma;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }


    public Iterator getLigacoes(){
        return ligacoes.iterator();
    }

    @Override
    public String toString() {
        return "Nome: " + nome + "," + "fantasma: " + fantasma ;
    }
}
