package Models;

public class Utilizador {
    private String nome;
    private int pontos;
    private String mapa;
    private int caminhos;

    public Utilizador(String nome, int pontos, String mapa, int caminhos) {
        this.nome = nome;
        this.pontos = pontos;
        this.mapa = mapa;
        this.caminhos = caminhos;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getPontos() {
        return pontos;
    }

    public void setPontos(int pontos) {
        this.pontos = pontos;
    }

    public String getMapa() {
        return mapa;
    }

    public void setMapa(String mapa) {
        this.mapa = mapa;
    }

    public int getCaminhos() {
        return caminhos;
    }

    public void setCaminhos(int caminhos) {
        this.caminhos = caminhos;
    }

    @Override
    public String toString() {
        return "Jogador: "+ nome +"; " +"Pontos: "+ pontos+ ";" +"Mapa:" + mapa +";" +  "Caminhos:" + caminhos +"\n";
    }
}
