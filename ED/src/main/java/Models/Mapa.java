package Models;

public class Mapa {
    private String nome;
    private int pontos;
    private Aposento[] aposentos;

    public Mapa(String nome, int pontos, Aposento[] aposentos) {
        this.nome = nome;
        this.pontos = pontos;
        this.aposentos = aposentos;
    }
    public Mapa(){}

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        nome = nome;
    }

    public int getPontos() {
        return pontos;
    }

    public void setPontos(int pontos) {
        this.pontos = pontos;
    }

    public Aposento[] getAposentos() {
        return aposentos;
    }

    public void setAposentos(Aposento[] aposentos) {
        this.aposentos = aposentos;
    }
}
